import os
from launch import LaunchDescription
import launch_ros.actions
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():
    return LaunchDescription([
        launch_ros.actions.Node(
            package ='hesai_lidar',
            executable ='hesai_lidar_node',
            name ='hesai_lidar_node',
            output ='screen',
            parameters=[
                {'pcap_file':       ''},
                {'server_ip':       '192.168.3.4'},
                {'lidar_recv_port': 2368},
                {'gps_port'  :      10110},
                {'start_angle':     0.0},
                {'lidar_type':      'PandarXT-16'},
                {'frame_id'  :      'obsequious/base_link/lidar_opt'},
                {'pcldata_type':    0},
                {'publish_type':    'points'}, # possible values: {'both', 'points', 'raw'}
                {'timestamp_type':  ''},
                {'data_type':       ''},
                {'lidar_correction_file': os.path.join(
                    get_package_share_directory('hesai_lidar'), 'config', 'PandarXT-16.csv'
                )},
                {'multicast_ip':    ''},
                {'coordinate_correction_flag': False},
                {'fixed_frame':     ''},
                {'target_frame_frame': ''}
            ],
            remappings=[('pandar', 'obsequious/lidar/points')]
        )
    ])
